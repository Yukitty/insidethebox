
--[[

Copyright (C) 2017 - Auke Kok <sofar@foo-projects.org>

"skybox" is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation; either version 2.1
of the license, or (at your option) any later version.

]]--

--
-- Builtin sky box textures and color/shadings, clouds
--

local skies = {
	{"DarkStormy", "#1f2226", 0.5, { density = 0.5, color = "#aaaaaae0", ambient = "#000000",
		height = 96, thickness = 32, speed = {x = 6, y = -6},}},
	{"CloudyLightRays", "#5f5f5e", 0.9, { density = 0.4, color = "#efe3d5d0", ambient = "#000000",
		height = 104, thickness = 24, speed = {x = 4, y = 0},}},
	{"FullMoon", "#24292c", 0.2, { density = 0.25, color = "#ffffff80", ambient = "#404040",
		height = 140, thickness = 8, speed = {x = -2, y = 2},}},
	{"SunSet", "#72624d", 0.4, { density = 0.2, color = "#f8d8e8e0", ambient = "#000000",
		height = 120, thickness = 16, speed = {x = 0, y = -2},}},
	{"ThickCloudsWater", "#a57850", 0.8, { density = 0.35, color = "#ebe4ddfb", ambient = "#000000",
		height = 104, thickness = 32, speed = {x = 4, y = 3},}},
	{"TropicalSunnyDay", "#f1f4ee", 1.0, { density = 0.25, color = "#fffffffb", ambient = "#000000",
		height = 120, thickness = 8, speed = {x = -2, y = 0},}},
}

--
-- API
--

skybox = {}

skybox.set = function(player, number)
	if number == 0 then
		skybox.clear(player)
	else
		local sky = skies[number]
		player:override_day_night_ratio(sky[3])
		player:set_sky(sky[2], "skybox", {
			sky[1] .. "Up.jpg",
			sky[1] .. "Down.jpg",
			sky[1] .. "Front.jpg",
			sky[1] .. "Back.jpg",
			sky[1] .. "Left.jpg",
			sky[1] .. "Right.jpg",
		}, true)
		player:set_clouds(sky[4])
	end
end

skybox.clear = function(player)
	player:override_day_night_ratio(nil)
	player:set_sky("white", "regular")
	player:set_clouds({
		density = 0.4,
		color = "#fff0f0e5",
		ambient = "#000000",
		height = 120,
		thickness = 16,
		speed = {x = 0, y = -2},
	})
end

minetest.register_privilege("skybox", {
	description = "Change sky box for yourself",
})

minetest.register_chatcommand("skybox", {
	params = "<skybox> or <number> or \"off\" or empty to list skyboxes",
	description = "Change your sky box set",
	privs = {skybox = true},
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		if not player then
			return
		end
		if param == nil or param == "" then
			minetest.chat_send_player(name, "Available sky boxes:")
			for _, v in ipairs(skies) do
				minetest.chat_send_player(name, v[1])
			end
			return
		elseif tonumber(param) ~= nil and tonumber(param) >= 1 and tonumber(param) <= #skies then
			skybox.set(player, tonumber(param))
			return
		elseif param == "off" or param == "0" then
			skybox.clear(player)
			return
		end
		for k, v in ipairs(skies) do
			if v[1] == param then
				skybox.set(player, k)
				return
			end
		end
		minetest.chat_send_player(name, "Could not find that sky box.")
	end
})

minetest.register_node("skybox:skybox", {
	description = "Skybox changer\nLeft/Right click to change the skybox",
	tiles = {
			"ThickCloudsWaterUp.jpg^[resize:16x16",
			"ThickCloudsWaterDown.jpg^[resize:16x16",
			"ThickCloudsWaterFront.jpg^[resize:16x16",
			"ThickCloudsWaterBack.jpg^[resize:16x16",
			"ThickCloudsWaterLeft.jpg^[resize:16x16",
			"ThickCloudsWaterRight.jpg^[resize:16x16",
		},
	groups = {node = 1, unbreakable = 1, trigger = 1},
	sounds = sounds.wood,
	on_punch = function(pos, node, puncher, pointed_thing)
		local name = puncher:get_player_name()
		if not puncher or not boxes.players_editing_boxes[name] then
			return
		end
		local meta = minetest.get_meta(pos)
		local s = meta:get_int("skybox") + 1
		if s > #skies then
			s = 0
		end
		minetest.chat_send_player(name, "skybox = " .. s)
		meta:set_int("skybox", s)
	end,
	on_rightclick = function(pos, node, puncher, itemstack, pointed_thing)
		local name = puncher:get_player_name()
		if not puncher or not boxes.players_editing_boxes[name] then
			return
		end
		local meta = minetest.get_meta(pos)
		local s = meta:get_int("skybox") - 1
		if s < 0 then
			s = #skies
		end
		minetest.chat_send_player(name, "skybox = " .. s)
		meta:set_int("skybox", s)
	end,
	on_trigger = function(pos)
		local box = boxes.find_box(pos)
		if not box then
			return
		end
		local p = minetest.get_player_by_name(box.name)
		if not p then
			return
		end
		local meta = minetest.get_meta(pos)
		local s = meta:get_int("skybox")
		skybox.set(p, s)
	end,
	on_untrigger = function(pos)
		local box = boxes.find_box(pos)
		if not box then
			return
		end
		local p = minetest.get_player_by_name(box.name)
		if not p then
			return
		end
		skybox.set(p, box.skybox or 0)
	end,
	on_reveal = function(name, pos)
		local meta = minetest.get_meta(pos)
		local s = meta:get_int("skybox")
		local t = "default"
		if skies[s] then
			t = skies[s][1]
		end
		minetest.chat_send_player(name, minetest.colorize( "#4444ff",
				"> skybox = " .. t))
	end,
})
