
--[[

Copyright (c) 2013, Diego Martinez (kaeza)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 - Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 - Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

--]]

-- override the IRC's mod `whereis` command to be more useful

if not irc or not irc.bot_commands["whereis"] then
	return
end

-- From irc/botcmds.lua
-- luacheck: ignore
irc.bot_commands["whereis"].func = function(_, args)
	if args == "" then
		return false, "Player name required."
	end
	local player = minetest.get_player_by_name(args)
	if not player then
		return false, "There is no player named '"..args.."'"
	end

	if boxes.players_editing_boxes[args] then
		return true, "Player " .. args .. " is creating a new box"
	else
		local box = boxes.players_in_boxes[args]
		if box then
			local id = box.box_id
			local bmeta = db.box_get_meta(id)
			local box_name = bmeta.meta.box_name
			return true, "Player " .. args .. " is playing box " .. id .. " - \"" .. box_name .. "\""
		else
			local pos = player:getpos()
			if pos.x < 1000 and pos.x > -1000 and
				pos.y < 200 and pos.y > -100 and
				pos.z < 1000 and pos.z > -1000 then
				return true, "Player " .. args .. " is in the lobby"
			end
		end
	end

	return true, "I don't know where " .. args .. " is"
end
