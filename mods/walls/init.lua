
--[[

walls - from minetest_game

Copyright (C) 2015 Auke Kok <sofar@foo-projects.org>

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation; either version 2.1 of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details:
https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html

]]--

walls = {}

walls.register = function(wall_name, wall_desc, wall_desc_middle, wall_texture, wall_sounds, groups, connect_groups)
	-- inventory node, and pole-type wall start item
	groups.wall = 1
	connect_groups[#connect_groups + 1] = "group:wall"
	minetest.register_node(wall_name, {
		description = wall_desc,
		drawtype = "nodebox",
		node_box = {
			type = "connected",
			fixed = {{-1/4, -1/2, -1/4, 1/4, 1/2, 1/4}},
			-- connect_bottom =
			connect_front = {{-3/16, -1/2, -1/2,  3/16, 3/8, -1/4}},
			connect_left = {{-1/2, -1/2, -3/16, -1/4, 3/8,  3/16}},
			connect_back = {{-3/16, -1/2,  1/4,  3/16, 3/8,  1/2}},
			connect_right = {{ 1/4, -1/2, -3/16,  1/2, 3/8,  3/16}},
		},
		collision_box = {
			type = "connected",
			fixed = {{-1/4, -1/2, -1/4, 1/4, 1, 1/4}},
			-- connect_bottom =
			connect_front = {{-1/4, -1/2, -1/2,  1/4, 1, -1/4}},
			connect_left = {{-1/2, -1/2, -1/4, -1/4, 1,  1/4}},
			connect_back = {{-1/4, -1/2,  1/4,  1/4, 1,  1/2}},
			connect_right = {{ 1/4, -1/2, -1/4,  1/2, 1,  1/4}},
		},
		connects_to = connect_groups,
		paramtype = "light",
		is_ground_content = false,
		tiles = { wall_texture, },
		walkable = true,
		groups = groups,
		sounds = wall_sounds,
	})

	minetest.register_node(wall_name .. "_middle", {
		description = wall_desc_middle,
		drawtype = "nodebox",
		node_box = {
			type = "fixed",
			fixed = {{-1/2, -1/2, -3/16, 1/2, 3/8, 3/16}},
		},
		collision_box = {
			type = "fixed",
			fixed = {{-1/2, -1/2, -3/16, 1/2, 1, 3/16}},
		},
		connect_sides = { "left", "right" },
		paramtype = "light",
		paramtype2 = "facedir",
		is_ground_content = false,
		tiles = { wall_texture, },
		walkable = true,
		groups = groups,
		sounds = wall_sounds,
	})

end

